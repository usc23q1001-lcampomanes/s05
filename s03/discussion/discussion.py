# username= input("Please enter your name: \n")
# print(f"Hi {username}! Welcome to Python Short Course")

# num1 = input("Enter 1st number: \n")
# num2= input("Enter 2nd number: \n")
# print(f"The sum of num1 and num2 is {num1 + num2}")

#Else-if chains
# test_num2= int(input("Please enter the 2nd test number \n"))

# if test_num2 > 0:
# 	print("The number is positive")
# elif test_num2 == 0:
# 	print("The number is zero")
# else:
# 	print("The number is negative")


test_num= int(input("Please enter the test number \n"))

if test_num%3 == 0 and test_num%5 == 0:
	print("The number is divisible by both 3 and 5")
elif test_num%3 == 0:
	print("The number is divisible by 3")
elif test_num%5 == 0:
	print("The number is divisible by 5")
else:
	print("The number is not divisible by 3 nor 5")